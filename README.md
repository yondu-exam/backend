## Description

[Nest](https://github.com/nestjs/nest) framework TypeScript starter repository.

## Installation

```bash
$ npm install
```
## Prepare .env
Create a database in mysql

then run this command
```bash
$ cp .env.example .env
 
```

After it, input the value of MYSQL_USER, MYSQL_PASSWORD, and MYSQL_DB


 ## Migrations and Seeding
Run below command to perform migration and update local database

```bash
#migration - update local database
$ knex migrate:latest

#seeding - seed our database
$ knex seed:run
 ```

## Running the app

```bash
# development
$ npm run start

# watch mode
$ npm run start:dev

```

## Test

```bash
# e2e tests
$ npm run test:e2e

```

## Default admin account
username: admin
password: password

