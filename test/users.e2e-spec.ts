import * as request from 'supertest';
import { Test } from '@nestjs/testing';
import { AppModule } from '../src/app.module';
import 'dotenv/config';
import { INestApplication } from '@nestjs/common';

const newUser = {
  firstName: 'John',
  lastName: 'Doe',
  address: 'Philippines',
  postCode: 4506,
  contactNumber: '09168314044',
  email: 'john.amen@gmail.com',
  username: 'john',
  password: 'newpassword',
};
let sampleUserId: string;
const ids = [];
let token: string;

describe('User', () => {
  let app: INestApplication;

  beforeAll(async () => {
    const moduleRef = await Test.createTestingModule({
      imports: [AppModule],
    }).compile();

    app = moduleRef.createNestApplication();
    await app.init();
  });

  it('/POST login', async () => {
    const res = await request(app.getHttpServer()).post('/login').send({
      username: 'admin',
      password: 'password',
    });

    expect(res.statusCode).toEqual(201);
    expect(res.body).toHaveProperty('access_token');
    token = res.body.access_token;
  });

  it('/POST user', async () => {
    const res = await request(app.getHttpServer())
      .post('/user')
      .set('Authorization', 'bearer ' + token)
      .send(newUser);

    expect(res.statusCode).toEqual(201);
    expect(res.body.data.firstName).toEqual('John');
    sampleUserId = res.body.data.id;
  });

  it('/GET user', async () => {
    const res = await request(app.getHttpServer())
      .get('/user')
      .set('Authorization', 'bearer ' + token);

    expect(res.statusCode).toEqual(200);
    ids.push(res.body.data[0].id);
    ids.push(res.body.data[1].id);
  });

  it('/PATCH user', async () => {
    const res = await request(app.getHttpServer())
      .patch(`/user/${sampleUserId}`)
      .set('Authorization', 'bearer ' + token)
      .send({ firstName: 'updatedFirstName' });

    expect(res.statusCode).toEqual(200);
    expect(res.body.data.firstName).toEqual('updatedFirstName');
    expect(res.body.data.id).toEqual(sampleUserId);
  });

  it('/DELETE user', async () => {
    const res = await request(app.getHttpServer())
      .delete(`/user/${sampleUserId}`)
      .set('Authorization', 'bearer ' + token);

    expect(res.statusCode).toEqual(200);
    expect(res.body.data).toEqual('Successfully deleted');
  });

  it('/DELETE users', async () => {
    const res = await request(app.getHttpServer())
      .delete(`/user`)
      .set('Authorization', 'bearer ' + token)
      .send({ ids });

    expect(res.statusCode).toEqual(200);
    expect(res.body.data).toEqual('Successfully deleted users');
  });

  afterAll(async () => {
    await app.close();
  });
});
