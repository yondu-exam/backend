import { Knex } from 'knex';

const tableName = 'users';

export async function up(knex: Knex) {
  return knex.schema.createTable(tableName, (t) => {
    t.uuid('id').primary();
    t.string('first_name');
    t.string('last_name');
    t.string('address');
    t.integer('post_code');
    t.string('contact_number');
    t.string('email');
    t.string('username');
    t.string('password');
  });
}

export async function down(knex: Knex) {
  return knex.schema.dropTable(tableName);
}
