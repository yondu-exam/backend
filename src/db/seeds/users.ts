import { Knex } from 'knex';
const userData = require('./user.json');

export async function seed(knex: Knex): Promise<void> {
  // Deletes ALL existing entries
  await knex('users').del();

  // Inserts seed entries
  await knex('users').insert(userData);
}
