import { Model } from 'objection';
import { v4 as uuid } from 'uuid';
import * as bcrypt from 'bcryptjs';

export class UserModel extends Model {
  id: string;
  firstName: string;
  lastName: string;
  address: string;
  postCode: number;
  contactNumber: string;
  email: string;
  username: string;
  password: string;

  static get tableName() {
    return 'users';
  }

  static get idColumn() {
    return 'id';
  }

  static get jsonSchema() {
    return {
      type: 'object',
      properties: {
        id: {
          type: 'string',
        },
        firstName: {
          type: 'string',
        },
        lastName: {
          type: 'string',
        },
        address: {
          type: 'string',
        },
        postCode: {
          type: 'number',
        },
        contactNumber: {
          type: 'string',
        },
        email: {
          type: 'string',
        },
        username: {
          type: 'string',
        },
        password: {
          type: 'string',
        },
      },
    };
  }

  async $beforeInsert() {
    this.id = uuid();
    // async hashPassword(){
    this.password = await bcrypt.hash(this.password, 8);
    // }
    // this.password= await bcrypt.has(this.password, 8)
  }

  async validatePassword(password: string): Promise<boolean> {
    return bcrypt.compare(password, this.password);
  }
}
