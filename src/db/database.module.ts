import Knex from 'knex';
import KnexConfig from '../../knexfile';
import { Module, Global } from '@nestjs/common';
import { Model } from 'objection';
import 'dotenv/config';
import { UserModel } from './models/user.model';
const models = [UserModel];

const modelProviders = models.map((model) => {
  return {
    provide: model.name,
    useValue: model,
  };
});

const providers = [
  ...modelProviders,
  {
    provide: 'KnexConnection',
    useFactory: async () => {
      const knex = Knex(KnexConfig[process.env.NODE_ENV]);
      Model.knex(knex);

      return knex;
    },
  },
];

@Global()
@Module({
  providers: [...providers],
  exports: [...providers],
})
export class DatabaseModule {}
