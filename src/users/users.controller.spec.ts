import { Test } from '@nestjs/testing';
import { UsersController } from './users.controller';
import { UsersService } from './users.service';
const userData = require('../db/seeds/user.json');

describe('UsersController', () => {
  let usersController: UsersController;
  let usersService: UsersService;

  beforeEach(async () => {
    const moduleRef = await Test.createTestingModule({
      controllers: [UsersController],
      providers: [UsersService],
    }).compile();

    usersService = moduleRef.get<UsersService>(UsersService);
    usersController = moduleRef.get<UsersController>(UsersController);
  });

  describe('getAllUsers', () => {
    it('should return an array of users', async () => {
      const result: any = [userData];
      jest.spyOn(usersService, 'getAllUsers').mockImplementation(() => result);
      expect(await usersController.getAllUsers()).toBe({ data: result });
    });
  });
});
