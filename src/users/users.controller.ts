import {
  Body,
  Controller,
  Delete,
  Get,
  Param,
  ParseArrayPipe,
  ParseUUIDPipe,
  Patch,
  Post,
  UseGuards,
} from '@nestjs/common';
import { AddUserDto, EditUserDto } from './user.dto';
import { UsersService } from './users.service';
import { JwtAuthGuard } from '../auth/jwt-auth.guard';

@Controller('user')
@UseGuards(JwtAuthGuard)
export class UsersController {
  constructor(private usersService: UsersService) {}

  // Add a new user
  @Post()
  async addUser(@Body() addUserDto: AddUserDto) {
    try {
      return { data: await this.usersService.addUser(addUserDto) };
    } catch (err) {
      throw new Error(err.message);
    }
  }

  // Edit a user
  @Patch('/:id')
  async editUser(
    @Param('id', ParseUUIDPipe) id: string,
    @Body() editUserDto: EditUserDto,
  ) {
    try {
      return { data: await this.usersService.editUser(id, editUserDto) };
    } catch (err) {
      throw new Error(err.message);
    }
  }

  // Delete a user
  @Delete('/:id')
  async deleteUser(@Param('id', ParseUUIDPipe) id: string) {
    try {
      return { data: await this.usersService.deleteUser(id) };
    } catch (err) {
      throw new Error(err.message);
    }
  }

  // View list of all users in the system
  @Get()
  async getAllUsers() {
    try {
      // TODO: add pagination
      return { data: await this.usersService.getAllUsers() };
    } catch (err) {
      throw new Error(err.message);
    }
  }

  // Allow multiple users to be removed
  @Delete()
  async deleteMultipleUsers(@Body('ids', ParseArrayPipe) ids: string[]) {
    try {
      return { data: await this.usersService.deleteMultipleUsers(ids) };
    } catch (err) {
      throw new Error(err.message);
    }
  }
}
