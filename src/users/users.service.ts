import { Inject, Injectable, NotFoundException } from '@nestjs/common';
import { ModelClass } from 'objection';
import { UserModel } from 'src/db/models/user.model';
import { AddUserDto, EditUserDto } from './user.dto';

@Injectable()
export class UsersService {
  constructor(
    @Inject('UserModel')
    private readonly usersModel: ModelClass<UserModel>,
  ) {}

  async addUser(addUserDto: AddUserDto) {
    try {
      const user = await this.usersModel.query().insert(addUserDto);
      delete user.password;

      return user;
    } catch (err) {
      throw new Error(err.message);
    }
  }

  async editUser(id: string, editUserEntity: EditUserDto) {
    try {
      const result = await this.usersModel
        .query()
        .patchAndFetchById(id, editUserEntity);
      if (!result) {
        throw new NotFoundException(`User doesn't exists`);
      }
      return result;
    } catch (err) {
      throw new Error(err.message);
    }
  }

  async deleteUser(id: string) {
    try {
      const result = await this.usersModel.query().deleteById(id);

      if (!result) {
        throw new NotFoundException(`User doesn't exists`);
      }

      return 'Successfully deleted';
    } catch (err) {
      throw new Error(err.message);
    }
  }

  async getAllUsers() {
    try {
      return this.usersModel.query();
    } catch (err) {
      throw new Error(err.message);
    }
  }

  async deleteMultipleUsers(ids: string[]) {
    try {
      const result = this.usersModel.query().delete().whereIn('id', ids);

      if (!result) {
        throw new NotFoundException(`User doesn't exists`);
      }
      return 'Successfully deleted users';
    } catch (err) {
      throw new Error(err.message);
    }
  }

  async findByUsername(username: string) {
    try {
      const result = await this.usersModel.query().findOne({ username });
      if (!result) {
        throw new NotFoundException(`User doesn't exists`);
      }
      return result;
    } catch (err) {
      throw new Error(err.message);
    }
  }
}
