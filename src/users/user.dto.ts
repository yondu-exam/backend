import { IsNumber, IsOptional, IsString } from 'class-validator';

class AddUserDto {
  @IsString()
  firstName: string;
  @IsString()
  lastName: string;
  @IsString()
  address: string;
  @IsNumber()
  postCode: number;
  @IsString()
  contactNumber: string;
  @IsString()
  email: string;
  @IsString()
  username: string;
  @IsString()
  password: string;
}

class EditUserDto {
  @IsString()
  @IsOptional()
  firstName: string;

  @IsString()
  @IsOptional()
  lastName: string;

  @IsString()
  @IsOptional()
  address: string;

  @IsNumber()
  @IsOptional()
  postCode: number;

  @IsString()
  @IsOptional()
  contactNumber: string;

  @IsString()
  @IsOptional()
  email: string;

  @IsString()
  @IsOptional()
  username: string;

  @IsString()
  @IsOptional()
  password: string;
}

export { AddUserDto, EditUserDto };
