// Update with your config settings.
import { Knex } from 'knex'
import 'dotenv/config';
import { knexSnakeCaseMappers } from 'objection';

export default {
  local: {
    client: process.env.MYSQL_CLIENT,
    connection: {
      host: process.env.MYSQL_HOST,
      database: process.env.MYSQL_DB,
      user: process.env.MYSQL_USER,
      password: process.env.MYSQL_PASSWORD,
    },
    migrations: {
      directory: __dirname + `/src/db/migrations`,
      stub: __dirname + `/src/db/migration.stub`,
    },
    pool: {
      min: 2,
      max: 12,
    },
    seeds: {
      directory: __dirname + `/src/db/seeds`,
    },
    ...knexSnakeCaseMappers()
  },
  
  test: {
    client: process.env.MYSQL_CLIENT,
    connection: {
      host: process.env.MYSQL_HOST,
      database: process.env.MYSQL_DB,
      user: process.env.MYSQL_USER,
      password: process.env.MYSQL_PASSWORD,
    },
    migrations: {
      directory: __dirname + `/src/db/migrations`,
      stub: __dirname + `/src/db/migration.stub`,
    },
    pool: {
      min: 2,
      max: 12,
    },
    seeds: {
      directory: __dirname + `/src/db/seeds`,
    },
    ...knexSnakeCaseMappers()
  }
} as Knex.Config;
